class Person:
    def __init__(self, name, age, social_security_number):
        # Initialisation des attributs de la classe Person
        self.name = name
        self.age = age
        self.social_security_number = social_security_number

    def get_name(self):
        # Méthode pour obtenir le nom de la personne
        return self.name

    def get_age(self):
        # Méthode pour obtenir l'âge de la personne
        return self.age

    def get_social_security_number(self):
        # Méthode pour obtenir le numéro de sécurité sociale de la personne
        return self.social_security_number


class Medication:
    def __init__(self, name, dosage):
        # Initialisation des attributs de la classe Medication
        self.name = name
        self.dosage = dosage

    def get_info(self):
        # Méthode pour obtenir les informations sur le médicament
        return f"{self.name} - Dosage: {self.dosage}"


class Illness:
    def __init__(self, name):
        # Initialisation des attributs de la classe Illness
        self.name = name
        self.medication_list = []

    def add_medication(self, medication):
        # Méthode pour ajouter un médicament à la liste des médicaments de la maladie
        self.medication_list.append(medication)

    def get_info(self):
        # Méthode pour obtenir les informations sur la maladie et les médicaments associés
        info = f"Illness: {self.name}\nMedications:"
        for medication in self.medication_list:
            info += f"\n{medication.get_info()}"
        return info


class Patient(Person):
    def __init__(self, name, age, social_security_number):
        # Appel du constructeur de la classe parente et initialisation des attributs supplémentaires
        super().__init__(name, age, social_security_number)
        self.patient_id = self.generate_patient_id()
        self.illness_list = []

    def generate_patient_id(self):
        # Méthode pour générer l'identifiant du patient
        return f"PID-{self.name[:3]}-{self.age}"

    def add_illness(self, illness):
        # Méthode pour ajouter une maladie à la liste des maladies du patient
        self.illness_list.append(illness)

    def get_info(self):
        # Méthode pour obtenir les informations sur le patient et ses maladies associées
        info = f"Patient ID: {self.patient_id}\nName: {self.name}\nAge: {self.age}\nSocial Security Number: {self.social_security_number}\nIllnesses:"
        for illness in self.illness_list:
            info += f"\n{illness.get_info()}"
        return info


class Care:
    def care_for_patient(self, patient):
        # Méthode pour fournir des soins au patient (à implémenter dans les sous-classes)
        pass

    def record_patient_visit(self, notes):
        # Méthode pour enregistrer la visite du patient
        print("Recorded notes:", notes)


class MedicalStaff(Person, Care):
    def __init__(self, name, age, social_security_number, employee_id):
        # Appel du constructeur de la classe parente et initialisation de l'attribut supplémentaire
        super().__init__(name, age, social_security_number)
        self.employee_id = employee_id

    def get_role(self):
        # Méthode pour obtenir le rôle du personnel médical (à implémenter dans les sous-classes)
        pass


class Doctor(MedicalStaff):
    def __init__(self, name, age, social_security_number, specialty):
        # Appel du constructeur de la classe parente et initialisation de l'attribut supplémentaire
        super().__init__(name, age, social_security_number, self.generate_employee_id(name))
        self.specialty = specialty

    @staticmethod
    def generate_employee_id(name):
        # Méthode statique pour générer l'identifiant de l'employé
        return f"DOC-{name[:3]}"

    def get_role(self):
        # Méthode pour obtenir le rôle du médecin
        return "Doctor"

    def care_for_patient(self, patient):
        # Méthode pour fournir des soins au patient (spécifique au médecin)
        print(f"{self.get_role()} {self.name} cares for {patient.get_name()}")


class Nurse(MedicalStaff):
    def __init__(self, name, age, social_security_number):
        # Appel du constructeur de la classe parente
        super().__init__(name, age, social_security_number, self.generate_employee_id(name))

    @staticmethod
    def generate_employee_id(name):
        # Méthode statique pour générer l'identifiant de l'employé
        return f"NRS-{name[:3]}"

    def get_role(self):
        # Méthode pour obtenir le rôle de l'infirmière
        return "Nurse"

    def care_for_patient(self, patient):
        # Méthode pour fournir des soins au patient (spécifique à l'infirmière)
        print(f"{self.get_role()} {self.name} cares for {patient.get_name()}")


if __name__ == "__main__":
    # Création d'instances pour tester le programme
    class Person:
        def __init__(self, name, age, social_security_number):
            # Initialisation des attributs de la classe Person
            self.name = name
            self.age = age
            self.social_security_number = social_security_number

        def get_name(self):
            # Méthode pour obtenir le nom de la personne
            return self.name

        def get_age(self):
            # Méthode pour obtenir l'âge de la personne
            return self.age

        def get_social_security_number(self):
            # Méthode pour obtenir le numéro de sécurité sociale de la personne
            return self.social_security_number


class Medication:
    def __init__(self, name, dosage):
        # Initialisation des attributs de la classe Medication
        self.name = name
        self.dosage = dosage

    def get_info(self):
        # Méthode pour obtenir les informations sur le médicament
        return f"{self.name} - Dosage: {self.dosage}"


class Illness:
    def __init__(self, name):
        # Initialisation des attributs de la classe Illness
        self.name = name
        self.medication_list = []

    def add_medication(self, medication):
        # Méthode pour ajouter un médicament à la liste des médicaments de la maladie
        self.medication_list.append(medication)

    def get_info(self):
        # Méthode pour obtenir les informations sur la maladie et les médicaments associés
        info = f"Illness: {self.name}\nMedications:"
        for medication in self.medication_list:
            info += f"\n{medication.get_info()}"
        return info


class Patient(Person):
    def __init__(self, name, age, social_security_number):
        # Appel du constructeur de la classe parente et initialisation des attributs supplémentaires
        super().__init__(name, age, social_security_number)
        self.patient_id = self.generate_patient_id()
        self.illness_list = []

    def generate_patient_id(self):
        # Méthode pour générer l'identifiant du patient
        return f"PID-{self.name[:3]}-{self.age}"

    def add_illness(self, illness):
        # Méthode pour ajouter une maladie à la liste des maladies du patient
        self.illness_list.append(illness)

    def get_info(self):
        # Méthode pour obtenir les informations sur le patient et ses maladies associées
        info = f"Patient ID: {self.patient_id}\nName: {self.name}\nAge: {self.age}\nSocial Security Number: {self.social_security_number}\nIllnesses:"
        for illness in self.illness_list:
            info += f"\n{illness.get_info()}"
        return info


class Care:
    def care_for_patient(self, patient):
        # Méthode pour fournir des soins au patient (à implémenter dans les sous-classes)
        pass

    def record_patient_visit(self, notes):
        # Méthode pour enregistrer la visite du patient
        print("Recorded notes:", notes)


class MedicalStaff(Person, Care):
    def __init__(self, name, age, social_security_number, employee_id):
        # Appel du constructeur de la classe parente et initialisation de l'attribut supplémentaire
        super().__init__(name, age, social_security_number)
        self.employee_id = employee_id

    def get_role(self):
        # Méthode pour obtenir le rôle du personnel médical (à implémenter dans les sous-classes)
        pass


class Doctor(MedicalStaff):
    def __init__(self, name, age, social_security_number, specialty):
        # Appel du constructeur de la classe parente et initialisation de l'attribut supplémentaire
        super().__init__(name, age, social_security_number, self.generate_employee_id(name))
        self.specialty = specialty

    @staticmethod
    def generate_employee_id(name):
        # Méthode statique pour générer l'identifiant de l'employé
        return f"DOC-{name[:3]}"

    def get_role(self):
        # Méthode pour obtenir le rôle du médecin
        return "Doctor"

    def care_for_patient(self, patient):
        # Méthode pour fournir des soins au patient (spécifique au médecin)
        print(f"{self.get_role()} {self.name} cares for {patient.get_name()}")


class Nurse(MedicalStaff):
    def __init__(self, name, age, social_security_number):
        # Appel du constructeur de la classe parente
        super().__init__(name, age, social_security_number, self.generate_employee_id(name))

    @staticmethod
    def generate_employee_id(name):
        # Méthode statique pour générer l'identifiant de l'employé
        return f"NRS-{name[:3]}"

    def get_role(self):
        # Méthode pour obtenir le rôle de l'infirmière
        return "Nurse"

    def care_for_patient(self, patient):
        # Méthode pour fournir des soins au patient (spécifique à l'infirmière)
        print(f"{self.get_role()} {self.name} cares for {patient.get_name()}")


if __name__ == "__main__":
    # Création d'instances pour tester le programme
    med1 = Medication("Aspirin", "500MG")
    med2 = Medication("Paracetamol", "500MG")
    med3 = Medication("Suppository", "500MG")
    illness1 = Illness("Fever")
    illness2 = Illness("Covid")
    illness3 = Illness("Grippe")
    illness1.add_medication(med1)
    illness2.add_medication(med2)
    illness3.add_medication(med3)

    patient1 = Patient("Leslie Richard", 31, "123-45-6789")
    patient2 = Patient("Sébastien Bernaz", 35, "125-67-6590")
    patient3 = Patient("Besson Damien", 39, "126-78-9087")
    patient1.add_illness(illness1)
    patient2.add_illness(illness2)
    patient3.add_illness(illness3)

    doctor1 = Doctor("Abrial", 60, "987-65-4321", "general practitioner")
    nurse1 = Nurse("Adeline", 25, "543-21-9876")
    doctor2 = Doctor("Jourdy", 68, "987-65-7645", "general practitioner")
    nurse2 = Nurse("Irene", 28, "543-21-9096")

    doctor1.care_for_patient(patient1)
    nurse1.care_for_patient(patient1)
    doctor1.care_for_patient(patient2)
    nurse1.care_for_patient(patient2)
    doctor2.care_for_patient(patient3)
    nurse2.care_for_patient(patient3)
    

